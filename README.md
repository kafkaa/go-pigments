## go-pigments

deploy system-exclusive MIDI components to the Novation Launch Key 

## Overview

go-pigments is a simple command line utility that reads an incoming MIDI data
stream for specific messages and then deploys custom system-exclusive configurations
for Arturia Pigments Synthesizer to the Novation Launch Key.

## Reference

+ [Novation Launchkey](https://novationmusic.com/en/keys/launchkey)
+ [Arturia Pigments](https://www.arturia.com/products/analog-classics/pigments/overview)
+ [MIDI Specs](https://www.midi.org/specifications)
+ [RTMIDI](https://www.music.mcgill.ca/~gary/rtmidi)
+ [Go rtmididrv](https://github.com/gomidi/rtmididrv)

