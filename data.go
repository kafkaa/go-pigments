package main

import "os"

// path to the novation custom components directory
var path string = os.Getenv("components")

type engine struct {
	analog    []string
	granular  []string
	wavetable []string
	harmonic  []string
	utility   []string
	slots     []byte
}

// the paths to each system-exclusive file for each engine
var engineOne = engine{
	[]string{
		"analog/engine-01/analog-pots-01.syx",
		"analog/engine-01/analog-pots-02.syx",
	},
	[]string{
		"granular/engine-01/grain-engine-01.syx",
		"granular/engine-01/grain-engine-02.syx",
	},
	[]string{
		"wavetable/engine-01/wavetable-pots-01.syx",
		"wavetable/engine-01/wavetable-pots-02.syx",
	},
	[]string{
		"harmonic/engine-01/harmonic-pots-01.syx",
		"harmonic/engine-01/harmonic-pots-02.syx",
	},
	[]string{
		"utility/utility-pots-01.syx",
		"utility/utility-pots-02.syx",
	},
	[]byte{1, 2},
}

var engineTwo = engine{
	[]string{
		"analog/engine-02/analog-pots-01.syx",
		"analog/engine-02/analog-pots-02.syx",
	},
	[]string{
		"granular/engine-02/grain-engine-01.syx",
		"granular/engine-02/grain-engine-02.syx",
	},
	[]string{
		"wavetable/engine-02/wavetable-pots-01.syx",
		"wavetable/engine-02/wavetable-pots-02.syx",
	},
	[]string{
		"harmonic/engine-02/harmonic-pots-01.syx",
		"harmonic/engine-02/harmonic-pots-02.syx",
	},
	[]string{
		"utility/utility-pots-01.syx",
		"utility/utility-pots-02.syx",
	},
	[]byte{3, 4},
}
