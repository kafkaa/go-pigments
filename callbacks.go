package main

import (
	"fmt"

	"gitlab.com/gomidi/midi/writer"
)

// scans the incoming midi stream for the specified selectors; loads the
// appropiate system-exclusive file; changes the 11th byte to select the
// corresponding custom slot on the launch key and then deploys the file.
func deployPreset(msg []byte, w *writer.Writer) {
	if files, slots := fetchPresets(msg); files != nil {
		for index, file := range files {
			data := load(fmt.Sprintf("%s%s", path, file))
			data[10] = slots[index] + 5
			if err := writer.SysEx(w, data); err == nil {
				fmt.Printf("Sent %s => Launchkey Slot %v\n", file, slots[index])
			} else {
				fmt.Println(err.Error())
			}
		}
	}
}
