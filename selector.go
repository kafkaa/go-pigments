/*selects the correct preset upon reception of spcific midi status byte.

cc numbers (msg[1]) that are assigned to and trigger each pigment's engine preset.

10: analog engine,
11: granular engine,
12: wavetable engine,
13: harmonic engine,
14: utility engine

cc values (msg[2]), determine the engine number; one or two.

64: engine one
127: engine two*/
package main

// this byte triggers the selector process
var status byte = 190

func fetchPresets(msg []byte) ([]string, []byte) {
	var slots []byte
	var files []string

	switch msg[0] {

	case status:
		switch msg[1] {

		case 15:
			switch msg[2] {

			case 32:
				files, slots = engineOne.analog, engineOne.slots

			case 64:
				files, slots = engineOne.granular, engineOne.slots

			case 96:
				files, slots = engineOne.wavetable, engineOne.slots

			case 127:
				files, slots = engineOne.harmonic, engineOne.slots
			}

		case 16:
			switch msg[2] {

			case 32:
				files, slots = engineTwo.analog, engineTwo.slots

			case 64:
				files, slots = engineTwo.granular, engineTwo.slots

			case 96:
				files, slots = engineTwo.wavetable, engineTwo.slots

			case 127:
				files, slots = engineTwo.harmonic, engineTwo.slots
			}

		case 17:
			switch msg[2] {

			case 64:
				files, slots = engineOne.utility, engineOne.slots

			case 127:
				files, slots = engineTwo.utility, engineTwo.slots
			}
		}

	default:
		return nil, nil

	}

	return files, slots
}
