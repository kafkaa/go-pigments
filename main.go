package main

import (
	"fmt"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/reader"
	"gitlab.com/gomidi/midi/writer"
	"gitlab.com/gomidi/rtmididrv"
)

func check(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func main() {
	drv, err := rtmididrv.New()
	check(err)

	defer drv.Close()
	ins, err := drv.Ins()
	check(err)

	outs, err := drv.Outs()
	check(err)

	in, out := selectPorts(&ins, &outs)

	clearTerminal()

	input := ins[in]
	fmt.Printf("Listening on %s\n", input)

	output := outs[out]
	fmt.Printf("Sending   to %s\n", output)

	check(input.Open())
	check(output.Open())

	defer input.Close()
	defer output.Close()

	mout := writer.New(output)

	// listen for MIDI
	recv := reader.New(
		reader.NoLogger(),
		reader.Each(func(_ *reader.Position, msg midi.Message) {
			deployPreset(msg.Raw(), mout)
		}),
	)

	ch := make(chan int, 10)
	go recv.ListenTo(input)
	<-ch

}
