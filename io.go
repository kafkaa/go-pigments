package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	"gitlab.com/gomidi/midi"
)

// clears the terminal
func clearTerminal() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

// loads a file into memory
func load(f string) []byte {
	b, err := ioutil.ReadFile(f)
	if err != nil {
		panic(err)
	}

	return b
}

// reads user input to select the appropiate midi ports
func selectPorts(ins *[]midi.In, outs *[]midi.Out) (int, int) {
	var input, output int

	printInPorts(*ins)
	fmt.Println("Choose Input Port: ")
	fmt.Scanln(&input)

	printOutPorts(*outs)
	fmt.Println("Choose Output Port: ")
	fmt.Scanln(&output)

	return input, output

}

// displays midi port options
func printPort(port midi.Port) {
	fmt.Printf("[%v] %s\n", port.Number(), port.String())
}

// displays available MIDI input ports
func printInPorts(ports []midi.In) {
	fmt.Printf("\nMIDI IN Ports Available\n")
	for _, port := range ports {
		printPort(port)
	}
	fmt.Printf("\n")
}

// displays available MIDI output ports
func printOutPorts(ports []midi.Out) {
	fmt.Printf("\nMIDI OUT Ports Available\n")
	for _, port := range ports {
		printPort(port)
	}
	fmt.Printf("\n")
}
